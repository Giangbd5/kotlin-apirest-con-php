<?php
/**
 *
 * @About:      Database connection manager class
 * @File:       Database.php
 * @Date:       $Date:$ Nov-2015
 * @Version:    $Rev:$ 1.0
 * @Developer:  Federico Guzman (federicoguzman@gmail.com)
 **/
class DbHandler {
 
    private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }
 
    public function createAuto($array)
    {
        //aqui puede incluir la logica para insertar el nuevo auto a tu base de datos
    }
	 public function obtener(){
		$rsl;
		try {
			$query = $this->conn->prepare("CALL procedimiento()");
			//$query->bindParam(1, $idEmpleado, PDO::PARAM_STR, 25);
			$query->execute();
			//$rsl= json_encode($query->fetchAll(PDO::FETCH_OBJ));
			$rsl= $query->fetchAll(PDO::FETCH_OBJ);	

			$query=null;
			$con=null;
		}catch(PDOException $ex){
			echo $ex->getMessage();
			die($ex->getMessage());
		};
		//echo $rsl;
		return $rsl;
	}
	 public function obtener2($param){
		 //echo $param['idUsuario'];
		$rsl;
		try {
			$query = $this->conn->prepare("CALL _per_Solicitud_obtenerWeb(?)");
			$query->bindParam(1, $param['idUsuario'], PDO::PARAM_STR, 25);
			$query->execute();
			//$rsl= json_encode($query->fetchAll(PDO::FETCH_OBJ));
			$rsl= $query->fetchAll(PDO::FETCH_OBJ);	

			$query=null;
			$con=null;
		}catch(PDOException $ex){
			echo $ex->getMessage();
			die($ex->getMessage());
		};
		//echo $rsl;
		return $rsl;
	}
}
 
?>