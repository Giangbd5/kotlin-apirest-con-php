/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50173
Source Host           : 127.0.0.1:3306
Source Database       : mensajeria

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2020-11-19 23:51:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tabla
-- ----------------------------
DROP TABLE IF EXISTS `tabla`;
CREATE TABLE `tabla` (
  `tipoSol` char(1) DEFAULT NULL,
  `idSolicitud` int(11) DEFAULT NULL,
  `nombreCliente` varchar(50) DEFAULT NULL,
  `ordenInterna` varchar(40) DEFAULT NULL,
  `fechaAsignacion` date DEFAULT NULL,
  `fechaEmision` date DEFAULT NULL,
  `nombreEstado` varchar(50) DEFAULT NULL,
  `nombreTipoSolicitud` varchar(50) DEFAULT NULL,
  `desNombre` varchar(50) DEFAULT NULL,
  `desDireccion` varchar(100) DEFAULT NULL,
  `desDistrito` varchar(100) DEFAULT NULL,
  `fechaRetiro` date DEFAULT NULL,
  `observaciones` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tabla
-- ----------------------------
INSERT INTO `tabla` VALUES ('D', '1', 'GLORIA', '20-00001', '2020-11-04', '2020-11-05', 'PENDIENTE', 'RECOJO', 'GLORIA', 'AV FABRICA DE GLORIA', 'MIRAFLORES', null, 'RECOGER DOCUMENTOS');
INSERT INTO `tabla` VALUES ('M', '2', 'NESTLE', '20-00002', '2020-11-03', '2020-11-04', 'PENDIENTE', 'TRASLADO', 'NESTLE', 'AV VENEZUELA', 'LIMA', null, 'DOCUMENTACION');

-- ----------------------------
-- Procedure structure for procedimiento
-- ----------------------------
DROP PROCEDURE IF EXISTS `procedimiento`;
DELIMITER ;;
CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `procedimiento`()
BEGIN
		-- Listar solicitudes de despacho
			SELECT 'D' AS tipoSol, idSolicitud, nombreCliente, ordenInterna, fechaAsignacion, fechaEmision,
				nombreEstado,
        nombreTipoSolicitud,
				desNombre,
				desDireccion,
				desDistrito,
				IFNULL(fechaRetiro,'SIN DEFINIR') AS fechaRetiro,
				observaciones
			FROM
			TABLA;
end
;;
DELIMITER ;
