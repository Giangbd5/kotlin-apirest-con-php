package com.surjaduanas.app3.adaptador


import com.surjaduanas.app3.modelo.Auto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiInterface {
    @Headers("Authorization:3d524a53c110e4c22463b10ed32cef9d")
    //@Headers("Content-type: json")
    @GET("auto")
    fun getPhotos(): Call<List<Auto>>

}