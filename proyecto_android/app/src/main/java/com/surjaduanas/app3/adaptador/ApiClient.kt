package com.surjaduanas.app3.adaptador

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    var BASE_URL:String="http://localhost/android3/v1/"
    val getClient: ApiInterface
        get() {
            val gson = GsonBuilder()
                .setLenient()
                .registerTypeAdapterFactory(CustomTypeAdapterFactory())
                .create()
            val interceptor = HttpLoggingInterceptor()
           interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)

                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            return retrofit.create(ApiInterface::class.java)

        }



}


private const val DATA = "autos"

class CustomTypeAdapterFactory : TypeAdapterFactory {
    override fun <T : Any?> create(gson: Gson?, type: TypeToken<T>?): TypeAdapter<T> {
        val delegate: TypeAdapter<T> = gson!!.getDelegateAdapter(this, type)
        val elementAdapter: TypeAdapter<JsonElement> = gson!!.getAdapter(JsonElement::class.java)

        return object : TypeAdapter<T>() {
            override fun write(out: com.google.gson.stream.JsonWriter?, value: T) {
                delegate.write(out, value)
            }

            override fun read(`in`: com.google.gson.stream.JsonReader?): T {
                var jsonElement: JsonElement = elementAdapter.read(`in`)

                if (jsonElement.isJsonObject) {
                    val jsonObject: JsonObject = jsonElement.asJsonObject
                    if (jsonObject.has(DATA)) {
                        jsonElement = jsonObject.get(DATA)
                    }
                }
                return delegate.fromJsonTree(jsonElement)
            }
        }.nullSafe()
    }
}