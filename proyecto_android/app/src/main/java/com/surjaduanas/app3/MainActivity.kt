package com.surjaduanas.app3

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.surjaduanas.app3.adaptador.ApiClient
import com.surjaduanas.app3.adaptador.DataAdapter
import com.surjaduanas.app3.modelo.Auto
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    lateinit var progerssProgressDialog: ProgressDialog
    var dataList = ArrayList<Auto>()
    lateinit var recyclerView: RecyclerView
    lateinit var adapter: DataAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById(R.id.recycler_view)

        //setting up the adapter
        recyclerView.adapter= DataAdapter(dataList, this)
        recyclerView.layoutManager= LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        progerssProgressDialog=ProgressDialog(this)
        progerssProgressDialog.setTitle("Loading")
        progerssProgressDialog.setCancelable(false)
        progerssProgressDialog.show()
        getData()

        recycler_view.setOnClickListener(){
            Toast.makeText(this, "sssssssss", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getData() {
        val call: Call<List<Auto>> = ApiClient.getClient.getPhotos()
        call.enqueue(object : Callback<List<Auto>> {

            override fun onResponse(call: Call<List<Auto>>?, response: Response<List<Auto>>?) {
                progerssProgressDialog.dismiss()
                dataList.addAll(response!!.body()!!)
                recyclerView.adapter?.notifyDataSetChanged()

            }

            override fun onFailure(call: Call<List<Auto>>?, t: Throwable?) {
                progerssProgressDialog.dismiss()
            }

        })
    }

}