package com.surjaduanas.app3.adaptador

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.surjaduanas.app3.R
import com.surjaduanas.app3.act_Detalle
import com.surjaduanas.app3.modelo.Auto
import android.widget.Toast.makeText as toast

class DataAdapter(private var dataList: List<Auto>, private val context: Context) : RecyclerView.Adapter<DataAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_home, parent, false))
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel=dataList.get(position)

        holder.textCliente.text=dataModel.nomCliente
        holder.textOrden.text=dataModel.nroOrden
        holder.textEstado.text=dataModel.nomEstado
        holder.textTipo.text=dataModel.nomEstado
        if (dataModel.tipoSoli=="M"){
            holder.imgTipo.setImageResource(R.drawable.icon_mensajero)
        }else if (dataModel.tipoSoli=="D"){
            holder.imgTipo.setImageResource(R.drawable.icon_despacho)
        }

        holder.itemView.setOnClickListener(View.OnClickListener {
            Toast.makeText(context, dataModel.nomCliente, Toast.LENGTH_SHORT).show()
            val detalle = Intent(context,act_Detalle::class.java).apply {

            }
            startActivity(context,detalle,null)
        })


    }

    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        lateinit var textCliente:TextView
        lateinit var textOrden:TextView
        lateinit var textEstado:TextView
        lateinit var textTipo:TextView
        lateinit var imgTipo: ImageView
        init {
            textCliente=itemLayoutView.findViewById(R.id.txtCliente)
            textOrden=itemLayoutView.findViewById(R.id.txtOrden)
            textEstado=itemLayoutView.findViewById(R.id.txtEstado)
            textTipo=itemLayoutView.findViewById(R.id.txtTipo)
            imgTipo=itemLayoutView.findViewById(R.id.imageView)

            
        }
    }
}