package com.surjaduanas.app3.modelo


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class DataModel(
    @Expose
    @SerializedName("error")
    val error: String,
    @Expose
    @SerializedName("message")
    val message: String,
    @Expose
    @SerializedName("autos")
    val autos: List<Auto>

)


data class Auto(
    @Expose
    @SerializedName("IdSdolicitud")
    val id: Integer,
    @Expose
    @SerializedName("tipoSol")
    val tipoSoli: String,
    @Expose
    @SerializedName("nombreCliente")
    val nomCliente: String,
    @Expose
    @SerializedName("ordenInterna")
    val nroOrden: String,
    @Expose
    @SerializedName("nombreEstado")
    val nomEstado: String
)